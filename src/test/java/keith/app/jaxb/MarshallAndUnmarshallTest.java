package keith.app.jaxb;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;

import keith.app.jaxb.domain.PersonType;
import keith.app.jaxb.domain.PersonsType;

import org.junit.Test;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class MarshallAndUnmarshallTest
{
    @Test
    public void test() throws Exception
    {
        try
        {
            testMarshaller();
            testUnmarshaller();
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
            throw e;
        }
        catch (DatatypeConfigurationException e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    private void testMarshaller() throws JAXBException, DatatypeConfigurationException
    {
        PersonsType persons = preparePersons();
        File file = new File("keith.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(PersonsType.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        XmlType xmlType = PersonsType.class.getAnnotation(XmlType.class);
        JAXBElement<PersonsType> jAXBElement = new JAXBElement<PersonsType>(new QName(xmlType.namespace(), "persons"), PersonsType.class,
                persons);
        jaxbMarshaller.marshal(jAXBElement, file);
        jaxbMarshaller.marshal(jAXBElement, System.out);
    }

    private PersonsType preparePersons() throws DatatypeConfigurationException
    {
        PersonType person1 = new PersonType();
        person1.setId(1);
        person1.setName("a");
        XMLGregorianCalendar xMLGregorianCalendar1 = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(2015, 11, 17, 0);
        person1.setBirthDate(xMLGregorianCalendar1);

        PersonType person2 = new PersonType();
        person2.setId(2);
        person2.setName("b");
        XMLGregorianCalendar xMLGregorianCalendar2 = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(2015, 11, 18, 0);
        person2.setBirthDate(xMLGregorianCalendar2);

        PersonsType persons = new PersonsType();
        persons.getPerson().add(person1);
        persons.getPerson().add(person2);
        return persons;
    }

    private void testUnmarshaller() throws JAXBException
    {
        File file = new File("keith.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(PersonsType.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        StreamSource streamSource = new StreamSource(file);
        JAXBElement<PersonsType> jAXBElement = jaxbUnmarshaller.unmarshal(streamSource, PersonsType.class);
        PersonsType persons = jAXBElement.getValue();
        System.out.println(persons);
    }
}
